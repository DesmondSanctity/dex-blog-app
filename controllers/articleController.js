var models = require('../models');
//var Article = require('../models/article');
//var Category = require('../models/category');
var async = require('async');

// Display article create form on GET.
exports.article_create_get = function(req, res, next) {
    // renders a article form
    res.render('forms/article_form', { title: 'Create Article', layout: 'layouts/detail' });
    console.log("Article form renders successfully");
};

// Handle article create on POST.
exports.article_create_post = async function(req, res, next) {

    // create a new article based on the fields in our article model
    // I have create two fields, but it can be more for your model

    const article = await models.Article.create({
        article_title: req.body.article_title,
        article_body: req.body.article_body,
        AuthorId: req.body.author_id

    });

    console.log("The saved article " + article.id);

    const category = await models.Category.findByPk(req.body.category_id);

    if (!category) {
        return res.status(400);
    }

    // ok category exist
    console.log("This is the category name we entered in front end " + category.category_name);

    const article_category = {
        article_id: article.id,
        category_id: category.id
    };

    // Create and save a productOrder
    //  const ProductCategorySaved = await models.ArticleCategory.create(article_category);
    await article.addCategory(category);

    console.log("Article created successfully");
    // check if there was an error during article creation
    res.redirect('/blog/articles');

};

//  Promise.all([User.create(), City.create()])
//     .then(([user, city]) => UserCity.create({userId: user.id, cityId: city.id}))


// Display article delete form on GET.
exports.article_delete_get = function(req, res, next) {
    models.Article.destroy({
        // find the article_id to delete from database
        where: {
            id: req.params.article_id
        }
    }).then(function() {
        // If an article gets deleted successfully, we just redirect to articles list
        // no need to render a page
        res.redirect('/blog/articles');
        console.log("Article deleted successfully");
    });
};

// Handle article delete on POST.
exports.article_delete_post = function(req, res, next) {
    models.Article.destroy({
        // find the article_id to delete from database
        where: {
            id: req.params.article_id
        }
    }).then(function() {
        // If an article gets deleted successfully, we just redirect to articles list
        // no need to render a page
        res.redirect('/blog/articles');
        console.log("Article deleted successfully");
    });

};

// Display article update form on GET.
exports.article_update_get = function(req, res, next) {
    // Find the article you want to update
    console.log("ID is " + req.params.article_id);
    models.Article.findByPk(
        req.params.article_id
    ).then(function(article) {
        // renders a article form
        res.render('forms/article_form', { title: 'Update Article', article: article, layout: 'layouts/detail' });
        console.log("Article update get successful");
    });

};

// Handle article update on POST.
exports.article_update_post = function(req, res, next) {
    console.log("ID is " + req.params.article_id);
    models.Article.update(
        // Values to update
        {
            article_title: req.body.article_title,
            article_body: req.body.article_body
        }, { // Clause
            where: {
                id: req.params.article_id
            }
        }
        //   returning: true, where: {id: req.params.article_id} 
    ).then(function() {
        // If an article gets updated successfully, we just redirect to articles list
        // no need to render a page
        res.redirect("/blog/articles");
        console.log("Article updated successfully");
    });
};

// Display detail page for a specific article.
exports.article_detail = function(req, res, next) {
    // find a article by the primary key Pk
    models.Article.findByPk(
        req.params.article_id, {
            include: models.Comment,

        }

    ).then(function(article) {
        // renders an inividual article details page
        res.render('pages/article_detail', { title: 'Article Details', article: article, layout: 'layouts/detail' });
        console.log("Article deteials renders successfully");
    });
};

// Display list of all articles.
exports.article_list = function(req, res, next) {
    // controller logic to display all articles
    models.Article.findAll({
        // Make sure to include the products
        include: [{
            model: models.Category,
            as: 'categories',
            required: false,
            // Pass in the Product attributes that you want to retrieve
            attributes: ['id', 'category_name'],
            through: {
                // This block of code allows you to retrieve the properties of the join table
                model: models.ArticleCategory
            }
        }]
    }).then(function(articles) {
        // renders a article list page
        console.log("rendering article list");
        res.render('pages/article_list', { title: 'Article List', articles: articles, layout: 'layouts/list' });
        console.log("Articles list renders successfully");
    });

};

// This is the blog homepage.
exports.index = function(req, res) {


    // find the count of articles in database
    models.Article.findAndCountAll().then(function(articleCount) {
        models.Author.findAndCountAll().then(function(authorCount) {
            models.Comment.findAndCountAll().then(function(commentCount) {
                models.Category.findAndCountAll().then(function(categoryCount) {



                    res.render('pages/index', { title: 'Homepage', articleCount: articleCount, authorCount: authorCount, commentCount: commentCount, categoryCount: categoryCount, layout: 'layouts/main' });

                    // res.render('pages/index_list_sample', { title: 'Article Details', layout: 'layouts/list'});
                    // res.render('pages/index_detail_sample', { page: 'Home' , title: 'Article Details', layout: 'layouts/detail'});

                });
            });
        });
    });




};
