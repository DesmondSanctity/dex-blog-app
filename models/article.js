'use strict';
module.exports = (sequelize, DataTypes) => {
    var Article = sequelize.define('Article', {
        article_title: DataTypes.STRING,
        article_body: DataTypes.TEXT,
        AuthorId: DataTypes.INTEGER
    });
    // create post association
    // a post will have an author
    // a field called AuthorId will be created in our post table inside the db
    Article.associate = function(models) {
        models.Article.belongsTo(models.Author, {
            onDelete: "CASCADE",
            foreignKey: {
                allowNull: false
            }
        });
        models.Article.belongsToMany(models.Category, {
            as: 'categories',
            through: 'ArticleCategories',
            foreignKey: 'article_id'
        });

        models.Article.hasMany(models.Comment);
    };




    return Article;
};
