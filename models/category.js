'use strict';
module.exports = (sequelize, DataTypes) => {
    var Category = sequelize.define('Category', {
        category_name: DataTypes.STRING
    });
    // create association between category and post
    // an category can have many posts
    Category.associate = function(models) {
        models.Category.belongsToMany(models.Article, {
            as: 'articles',
            through: 'ArticleCategories',
            foreignKey: 'category_id'

        });
    };

    return Category;
};
